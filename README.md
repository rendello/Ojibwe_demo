
![Cover image for Ojibwe Dictionary Scraping Demonstration](Images/cover.png "Ojibwe Dictionary Scraping Demonstration")


[![black badge](Images/black_psf_badge.svg "")](https://github.com/psf/black)
[![mypy badge](Images/mypy_psf_badge.svg "")](https://github.com/python/mypy)

This project is a proof-of-concept that scrapes Ojibwe language examples from
[The Ojibwe People's Dictionary](https://ojibwe.lib.umn.edu/) and the
[Nishnaabemwin Dictionary](https://dictionary.nishnaabemwin.atlas-ling.ca).

As will be seen later in this document, the scraping strategies for each site
were completely different by necessity.

![Header for "Rationale" section](Images/heading_rationale.png "Rationale")

This small project was created for demonstration purposes as a part of my
application.

Although this project is new, I have done some limited web-scraping of Ojibwe
languages resources before. I hope to soon have a large enough corpus of
standardized Ojibwe texts in order to allow me to do some machine learning,
which could help automatic transcription efforts of this endangered language.

![Header for "Strategies" section](Images/heading_strategies.png "Strategies")


### The Ojibwe People's Dictionary: BeautifulSoup

The Ojibwe People's Dictionary is a wonderful project that goes very light on
Javascript, tending to do the bulk of processing in the back end and relaying
static pages. As such, its "browse" section is easily navigatable with Python's
BeautifulSoup library, which requires only a copy of a given page's HTML to
process.

With a small team running it and 30 000 + entries, the strategy is simply to
walk the browse section page by page, indexing each entry's URL, and then
scrape all of the Ojibwe words from each page individually. Each page has a
lemma, as in the word being defined, and can optionally have inflected and
conjugated forms, as well as example sentences.

### The Nishnaabemwin Dictionary: Selenium Webdriver

Unlike the OPD, the Nishnaabemwin site is a "thick" site, doing the bulk of its
processing inside the client's web browser. When a user uses the "browse" page,
they can see twenty entries at a time, the headings of which can be clicked.
When clicked, the definitions, inflections, example, and related words are sent
to the browser in JSON form.

Although requesting and parsing the JSON files directly is certainly possible,
it has proven trivially simple to pilot a real web-browser through Selenium to
scrape the data.

For each page of the "browse" section, all entries are clicked, and when the
data for each is loaded, it's scraped in one pass. The next page is then
navigated to, where the process continues.

*Note: Out of respect for the limited resources of these online dicitonaries,
downloading is single-threaded.*

![Header for "Projects" section](Images/heading_projects.png "Projects")


#### Python & OAuth
- **[Auto Tweetcart](https://gitlab.com/rendello/auto_tweetcart)**: A Python Twitter bot
  ([link](https://twitter.com/auto_tweetcart/)) that securely runs Lua
  programs that are tweeted at it and displays a video of the result as a reply.
- **[Simón](https://gitlab.com/rendello/simon)**: A Python Discord chat
  application based on the electronic game "Simon".
- **[Poké Poké](https://gitlab.com/rendello/poke-poke-bot)**: A Python Discord
  chat application that lets users play a competitive game of "Who's That
  Pokémon". Dynamically creates shrouded versions of the characters using the
  Pillow Python library and images from the Pokémon Wiki

#### Understanding of C
- **Bull!ts**: A "Shoot 'Em Up" game in a JIT variant of C (current project).
- Various graphical demos in C: [Example Here](https://www.youtube.com/watch?v=W-wpUcDFZaI)
